package com.unju.tpCardozo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpCardozoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpCardozoApplication.class, args);
	}

}
