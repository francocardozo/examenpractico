/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.tpCardozo.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Franco
 */
@Entity
@Table(name="prestamos")
public class Prestamos {
    
    @Id
    @Column(name="idPrestamo")
    private int idPrestamo;
    
    @ManyToOne
    @JoinColumn(name="nroId")
    private Estudiantes nroId;
    
    @ManyToOne
    @JoinColumn(name="isbn")
    private Libros isbn;
    
    @Column(name="fechaPrestamo")
    private String fechaPrestamo;

    public Prestamos(int idPrestamo, Estudiantes nroId, Libros isbn, String fechaPrestamo) {
        this.idPrestamo = idPrestamo;
        this.nroId = nroId;
        this.isbn = isbn;
        this.fechaPrestamo = fechaPrestamo;
    }

    public Prestamos() {
    }

    public int getIdPrestamo() {
        return idPrestamo;
    }

    public void setIdPrestamo(int idPrestamo) {
        this.idPrestamo = idPrestamo;
    }

    public Estudiantes getNroId() {
        return nroId;
    }

    public void setNroId(Estudiantes nroId) {
        this.nroId = nroId;
    }

    public Libros getIsbn() {
        return isbn;
    }

    public void setIsbn(Libros isbn) {
        this.isbn = isbn;
    }

    public String getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(String fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    
    
    
    
}
